var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var log4js = require('log4js');
var logger = log4js.getLogger();
var validate = require('validate.js');
var re = ['invalid'];
var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'student'
});
router.get('/sri/:name/:id', function(req, res) {
    console.log(req.params);
    console.log(req.query);
    console.log("getting narendra");
})
router.get('/', function(req, res) {

    pool.query('SELECT * FROM register', function(err, data) {
        if (err) throw err;
        console.log('The solution is: ' + data);
        // res.send({name:'srilakshmi'});
        res.send(data);
    });
});
router.post('/create', function(req, res) {
    console.log("in routese");
    console.log(req.body.my_file);
    var obj = req.body;
    var value = {
        firstname: obj.firstname,
        lastname: obj.lastname,
        username: obj.username,
        password: obj.password,
        mobile: obj.mobile,
        email: obj.email,
        profile_pic_url:obj.my_file
    }
    pool.query('insert into student_details set ?', value, function(err, data) {
        if (err) {
            logger.error("error")
            res.send({status:400,flag:"fail"})
        } else {
            console.log(data);
            logger.info("values entered successfuly");
            res.send({status:200,flag:"success",name:obj.username})
        }
    });
});
router.get('/login/getting/:email', function(req, res) {
    console.log("in verifing");
    console.log("hello");
    console.log(req.params.email);
    var obj = req.body;
    pool.query('select * from student.student_details where mail =? and password =?', [req.body.email, req.body.password], function(err, result) {
        if (err) {
            throw err;
        }
        if (result) {
            logger.info("Got the result")
            if (result.length > 0) {
              logger.info("Check the result")
                res.send(result);
            } else {
              logger.info("No details found")
                res.send(result);
            }
        }
    });
});
router.post('/getdetails', function(req, res) {
    console.log(req.body.getname);
    var cons = {
        getname: {
            length: {
                minimum: 1,
                maximum: 10
            }
        }
    };
    var valid = validate(req.body, cons);
    console.log(valid);
    pool.query('select * from register where id = ?', [req.body.getname], function(err, data) {
        if (err) throw err;
        if (data.length >= 1) {
            data[0].na = 'true';
            res.send(data);
            console.log(data.length, data);
        } else {
            // var obj={};
            // obj.na='false';
            res.send(data);
        }
        //console.log(data[0].na);
    });
});
router.post('/donate', function(req, res) {
    console.log("in router");
    var value = {
        goal: req.body.donate
    }
    pool.query('insert into student.fund set ?', value, function(err, result) {
        if (err) {
            console.log("error");
        } else {
            res.send(result);

            console.log(result);
        }

    });
});
router.get('/getdonations', function(req, res) {
    pool.query('select * from student.fund', function(err, data) {
        if (err) {
            throw err;
        } else {
            res.send(data);
            console.log(data);
        }
    });
});
module.exports = router;
